"use strict";
class WebForm {
    submit() {
        
        var date = new Date();
        
        var submittedForm = {
            roomNumber:document.getElementById("roomNumber").value.toLowerCase(),
            buildingAddress:document.getElementById("buildingAddress").value.toLowerCase(),
            lightsOn:document.getElementById("lightsOn").value,
            heatingCoolingOn:document.getElementById("airConditioningHeatingOn").value,
            computersUsed:document.getElementById("numberOfComputersInUse").value,
            computersTotal:document.getElementById("totalComputers").value,
            seatsUsed:document.getElementById("numberOfSeatsInUse").value,
            seatsTotal:document.getElementById("totalSeats").value,
            dateChecked: date.toISOString()
        }
        
        
        
        var validate = {
            roomNumberValid: function() {
                if (submittedForm.roomNumber===""){
                    document.getElementById("msg5").innerHTML="*PLEASE INPUT VALUE!";
                }
                else {
                    document.getElementById("msg5").innerHTML="";
                    validate.buildingAddress()
                }
            },
            
            buildingAddress: function() {
                if (submittedForm.buildingAddress===""){
                    document.getElementById("msg6").innerHTML="*PLEASE INPUT VALUE!";
                }
                else {
                    document.getElementById("msg6").innerHTML="";
                    validate.totalComputerValid()
                }
            },
            
            totalComputerValid: function() {
                if (submittedForm.computersTotal<0){
                    document.getElementById("msg2").innerHTML="*VALUE MUST BE POSITIVE!";
                }
                else if (submittedForm.computersTotal===""){
                    document.getElementById("msg2").innerHTML="*PLEASE INPUT VALUE!";
                }
                else {
                    document.getElementById("msg2").innerHTML="";
                    validate.usedComputerValid()
                }
            },
            
            usedComputerValid: function () {
                if (submittedForm.computersUsed<0){
                    document.getElementById("msg1").innerHTML="*VALUE MUST BE POSITIVE!";
                }
                else if (submittedForm.computersUsed===""){
                    document.getElementById("msg1").innerHTML="*PLEASE INPUT VALUE!";
                }
                else if (submittedForm.computersTotal-submittedForm.computersUsed <0 ){
                    document.getElementById("msg1").innerHTML="*VALUE MUST BE LESS THAN TOTAL COMPUTERS!";
                }
                else {
                    document.getElementById("msg1").innerHTML="";
                    validate.totalSeatsValid();
                }
            },
            
            totalSeatsValid: function() {
                if (submittedForm.seatsTotal <0){
                    document.getElementById("msg4").innerHTML="*VALUE MUST BE POSITIVE!";
                }
                else if (submittedForm.seatsTotal===""){
                    document.getElementById("msg4").innerHTML="*PLEASE INPUT VALUE!";
                }
                else {
                    document.getElementById("msg4").innerHTML="";
                    validate.usedSeatsValid();
                }
            },
            
            usedSeatsValid: function () {
                if (submittedForm.seatsUsed<0){
                    document.getElementById("msg3").innerHTML="*VALUE MUST BE POSITIVE!";
                }
                else if (submittedForm.seatsTotal-submittedForm.seatsUsed <0 ){
                    document.getElementById("msg3").innerHTML="*VALUE MUST BE LESS THAN TOTAL SEATS!";
                }
                else if (submittedForm.seatsUsed===""){
                    document.getElementById("msg3").innerHTML="*PLEASE INPUT VALUE!";
                }
                else {
                    document.getElementById("msg3").innerHTML="";
                    var parameter= Object.values(submittedForm);
                    var roomConfirmed= new RoomUsage(parameter);
                    console.log(roomConfirmed.roomDetails);
                }
            }
        }
        
        validate.roomNumberValid();    
        
         
    }
    
    clear(){
        document.getElementById("myForm").reset();
    }
    
    
}

